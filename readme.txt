
This module opens a BackDoor into Scilab workspace. 

It works by creating a new thread that listens on requests on a TCP port.
Each string send to that thread is executed in the Scilab workspace and a result
is sent back to the caller when requested.

For testing load the module (when the module is loaded you should see something
like:
  
  BackDoor: listening to commands on TCP port 28020

and then try opening a telnet connection 
  
  telnet 120.0.0.1 28020

and start writing Scilab commands as you would on the command window (with a new
line in the end). NOTE: the port used is 27020 in windows and 27020 + UID in
Linux. This is to allow multiple users using the module.

The result of the calculation is saved in the bd_result variable in Scilab.

If you need to run a command that shouldn't be saved in bd_result you can prefix
your command with "\", for example "\load('file.dat')". 

Also if you type a '@' you will get the value of bd_result(in binary form of a C
double). If you type "?" you will get the value of bd_result in text.

The intention of this module is to use it with PowerDEVS (https://sourceforge.net/projects/powerdevs)
 in order to process simulation results in the Scilab enviroment.
