/* ==================================================================== */
/* This file is released into the public domain */
/* ==================================================================== */
#ifndef __BACKDOOR_H__
#define __BACKDOOR_H__
#ifdef  __linux__
#include "Scierror.h"
#include "MALLOC.h"
#else
#include <windows.h>
#include <tchar.h>
#include <strsafe.h>
#endif
//#define __USE_DEPRECATED_STACK_FUNCTIONS__
//#include "call_scilab.h"
//#include "stack-c.h"
#include "api_scilab.h"

int openBackDoor();
int closeBackDoor();
#endif /* __CSUM_H__ */

